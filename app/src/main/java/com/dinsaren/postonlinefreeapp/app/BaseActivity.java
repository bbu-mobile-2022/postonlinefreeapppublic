package com.dinsaren.postonlinefreeapp.app;

import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {
    @Override
    protected void onResume() {
        super.onResume();
    }

    protected void showMessage(String message){
        Toast.makeText(this,message, Toast.LENGTH_LONG).show();
    }

}
