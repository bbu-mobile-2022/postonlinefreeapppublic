package com.dinsaren.postonlinefreeapp.features.home;

import com.dinsaren.postonlinefreeapp.models.res.Category;
import com.dinsaren.postonlinefreeapp.models.res.Post;
import com.dinsaren.postonlinefreeapp.models.res.Slide;
import com.dinsaren.postonlinefreeapp.view.BaseView;

import java.util.List;

public interface HomeView extends BaseView {
    void setCategoryList(List<Category> categoryList);
    void setSlideList(List<Slide> slideList);
    void setProduct(List<Post> postList);
}
