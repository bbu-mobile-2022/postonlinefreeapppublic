package com.dinsaren.postonlinefreeapp.features.register.view;

import com.dinsaren.postonlinefreeapp.models.User;
import com.dinsaren.postonlinefreeapp.view.BaseView;

public interface LoginView extends BaseView {
    void onLoginSuccess(User user);
}
