package com.dinsaren.postonlinefreeapp.features.category.sub;

import com.dinsaren.postonlinefreeapp.models.res.SubCategory;
import com.dinsaren.postonlinefreeapp.view.BaseView;

import java.util.List;

public interface SubCategoryView extends BaseView {
    void setDataSubCategory(List<SubCategory> list);
}
