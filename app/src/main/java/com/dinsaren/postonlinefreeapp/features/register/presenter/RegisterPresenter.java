package com.dinsaren.postonlinefreeapp.features.register.presenter;

import androidx.annotation.NonNull;

import com.dinsaren.postonlinefreeapp.features.register.view.RegisterView;
import com.dinsaren.postonlinefreeapp.models.req.RegisterReq;
import com.dinsaren.postonlinefreeapp.models.res.RegisterRes;
import com.dinsaren.postonlinefreeapp.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterPresenter {
    private RegisterView view;

    public RegisterPresenter(RegisterView view) {
        this.view = view;
    }

    public void register(RegisterReq req){
        view.onLoading("On Loading Register...");
        try {
            Call<RegisterRes> registerResCall = Utils.getPofApis().authRegister(req);
            registerResCall.enqueue(new Callback<RegisterRes>() {
                @Override
                public void onResponse(@NonNull Call<RegisterRes> call, @NonNull Response<RegisterRes> response) {
                    view.onHideLoading("");
                    if (response.code() == 400) {
                        view.onError("Error: Username is already taken!");
                    }
                    if (response.code() == 200 && response.isSuccessful()) {
                        view.onSuccess("Your register are success");
                    }

                }

                @Override
                public void onFailure(@NonNull Call<RegisterRes> call, @NonNull Throwable t) {
                    view.onHideLoading("");
                    view.onInternalServerError(t.getLocalizedMessage());
                }
            });
        }catch (Exception e){
            view.onHideLoading("");
            view.onError(e.getLocalizedMessage());
        }

    }

}
