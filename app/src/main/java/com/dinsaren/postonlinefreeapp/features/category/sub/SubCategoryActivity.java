package com.dinsaren.postonlinefreeapp.features.category.sub;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.dinsaren.postonlinefreeapp.R;
import com.dinsaren.postonlinefreeapp.adapters.SubCategoryAdapter;
import com.dinsaren.postonlinefreeapp.app.BaseActivity;
import com.dinsaren.postonlinefreeapp.models.res.SubCategory;

import java.util.List;

public class SubCategoryActivity extends BaseActivity implements SubCategoryView{
    private RecyclerView recyclerViewSubCategory;
    private ProgressBar progressBar;
    private SubCategoryPresenter subCategoryPresenter;
    private SubCategoryAdapter subCategoryAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);
        recyclerViewSubCategory = findViewById(R.id.recyclerViewSubCategory);
        progressBar = findViewById(R.id.progressBar);
        subCategoryPresenter = new SubCategoryPresenter(this);
        Intent intent = getIntent();
        int id = intent.getIntExtra("ID",0);
        String name = intent.getStringExtra("NAME");
        if(id != 0) {
            subCategoryPresenter.getAllSubCategories(id);
            setTitle(name);
        }

    }

    @Override
    public void setDataSubCategory(List<SubCategory> list) {
        if(!list.isEmpty()){
            subCategoryAdapter = new SubCategoryAdapter(this,list);
            recyclerViewSubCategory.setLayoutManager(new GridLayoutManager(this,
                    1, RecyclerView.HORIZONTAL, false));
            recyclerViewSubCategory.setAdapter(subCategoryAdapter);

        }
    }

    @Override
    public void onLoading(String message) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideLoading(String message) {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onError(String message) {
        showMessage(message);
    }

    @Override
    public void onSuccess(String message) {
        showMessage(message);
    }

    @Override
    public void onInternalServerError(String message) {
        showMessage(message);
    }
}