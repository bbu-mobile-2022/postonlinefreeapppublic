package com.dinsaren.postonlinefreeapp.features.register.presenter;

import com.dinsaren.postonlinefreeapp.features.register.view.LoginView;
import com.dinsaren.postonlinefreeapp.models.User;
import com.dinsaren.postonlinefreeapp.models.req.LoginReq;
import com.dinsaren.postonlinefreeapp.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter {
    private LoginView view;

    public LoginPresenter(LoginView view) {
        this.view = view;
    }

    public void login(String username, String password){
        view.onLoading("Loading...");
        LoginReq req = new LoginReq();
        req.setPhone(username);
        req.setPassword(password);
        Call<User> loginCall = Utils.getPofApis().authLogin(req);
        loginCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                view.onHideLoading("Hiding..");
                if(response.code() == 401){
                    view.onError("Your username and password is incorrect");
                }
                if(response.isSuccessful() && null != response.body()){
                    view.onSuccess("Login Success");
                    view.onLoginSuccess(response.body());
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                view.onHideLoading("Hide");
                view.onInternalServerError("Connection to server timeout!");
            }
        });

    }
}
