package com.dinsaren.postonlinefreeapp.features.category.sub;

import com.dinsaren.postonlinefreeapp.models.res.SubCategory;
import com.dinsaren.postonlinefreeapp.utils.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubCategoryPresenter {
    private SubCategoryView view;

    public SubCategoryPresenter(SubCategoryView view) {
        this.view = view;
    }

    public void getAllSubCategories(Integer id){
        view.onLoading("");
        Call<List<SubCategory>> call = Utils.getPofApis().getAllSubCategories(id);
        call.enqueue(new Callback<List<SubCategory>>() {
            @Override
            public void onResponse(Call<List<SubCategory>> call, Response<List<SubCategory>> response) {
                view.onHideLoading("");
                if(response.isSuccessful() && null != response.body()){
                    view.onSuccess("Data Success");
                    view.setDataSubCategory(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<SubCategory>> call, Throwable t) {
                view.onHideLoading("");
                view.onInternalServerError("Connection timeout!");
            }
        });

    }
}
