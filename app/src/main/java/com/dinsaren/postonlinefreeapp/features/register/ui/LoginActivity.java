package com.dinsaren.postonlinefreeapp.features.register.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.dinsaren.postonlinefreeapp.R;
import com.dinsaren.postonlinefreeapp.app.BaseActivity;
import com.dinsaren.postonlinefreeapp.features.register.presenter.LoginPresenter;
import com.dinsaren.postonlinefreeapp.features.register.view.LoginView;
import com.dinsaren.postonlinefreeapp.models.User;
import com.dinsaren.postonlinefreeapp.utils.local.UserSharedPreference;

public class LoginActivity extends BaseActivity implements LoginView {
    private ProgressBar progressBar;
    private EditText etUsername, etPassword;
    private Button btnLogin;
    private LoginPresenter loginPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.initView();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = etUsername.getText().toString().trim();
                String password = etPassword.getText().toString().trim();

                if(username.equals("")){
                    showMessage("Username is empty");
                    return;
                }
                if(password.equals("")){
                    showMessage("Password is empty");
                    return;
                }
                loginPresenter.login(username, password);

            }
        });
    }

    private void initView(){
        loginPresenter = new LoginPresenter(this);
        progressBar = findViewById(R.id.progressBar);
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);
    }

    @Override
    public void onLoginSuccess(User user) {
        UserSharedPreference.setUser(LoginActivity.this,
                user);
        finish();
    }

    @Override
    public void onLoading(String message) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideLoading(String message) {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onError(String message) {
        showMessage(message);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(String message) {
        showMessage(message);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onInternalServerError(String message) {
        showMessage(message);
        progressBar.setVisibility(View.GONE);
    }

}