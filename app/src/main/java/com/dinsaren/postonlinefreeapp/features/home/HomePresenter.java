package com.dinsaren.postonlinefreeapp.features.home;

import com.dinsaren.postonlinefreeapp.models.res.Category;
import com.dinsaren.postonlinefreeapp.models.res.Post;
import com.dinsaren.postonlinefreeapp.models.res.Slide;
import com.dinsaren.postonlinefreeapp.utils.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePresenter {
    private HomeView view;

    public HomePresenter(HomeView view) {
        this.view = view;
    }
    public void getAllCategories(){
        view.onLoading("");
        Call<List<Category>> categoryResCall = Utils.getPofApis().getAllCategories();
        categoryResCall.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                view.onHideLoading("");
                if(response.isSuccessful() && null != response.body()) {
                    view.onSuccess("Get Data Success");
                    view.setCategoryList(response.body());
                }else{
                    view.onError("Get Data un Success");
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                view.onHideLoading("");
                view.onInternalServerError("Connection timeout!");
            }
        });
    }

    public void getAllSlides(){
        view.onLoading("");
        Call<List<Slide>> slideResCall = Utils.getPofApis().getAllSlide();
        slideResCall.enqueue(new Callback<List<Slide>>() {
            @Override
            public void onResponse(Call<List<Slide>> call, Response<List<Slide>> response) {
                view.onHideLoading("");
                if(response.isSuccessful() && null != response.body()){
                    view.setSlideList(response.body());
                }else{
                    view.onError("Error get all slide");
                }
            }

            @Override
            public void onFailure(Call<List<Slide>> call, Throwable t) {
                view.onHideLoading("");
                view.onInternalServerError("Connection timeout !"+t.getLocalizedMessage());
            }
        });
    }
    public void getAllProducts(){
        view.onLoading("");
        Call<List<Post>> callPost = Utils.getPofApis().getAllProducts();
        callPost.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                view.onHideLoading("");
                if(response.isSuccessful() && null != response.body()){
                    view.setProduct(response.body());
                }else{
                    view.onError("Error get product list");
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                view.onError("");
                view.onInternalServerError("Connection timeout !"+t.getLocalizedMessage());
            }
        });
    }
}
