package com.dinsaren.postonlinefreeapp.view;

public interface BaseView {
    void onLoading(String message);
    void onHideLoading(String message);
    void onError(String message);
    void onSuccess(String message);
    void onInternalServerError(String message);

}
