package com.dinsaren.postonlinefreeapp.api;


import com.dinsaren.postonlinefreeapp.models.User;
import com.dinsaren.postonlinefreeapp.models.req.LoginReq;
import com.dinsaren.postonlinefreeapp.models.req.RegisterReq;
import com.dinsaren.postonlinefreeapp.models.res.Category;
import com.dinsaren.postonlinefreeapp.models.res.Post;
import com.dinsaren.postonlinefreeapp.models.res.RegisterRes;
import com.dinsaren.postonlinefreeapp.models.res.Slide;
import com.dinsaren.postonlinefreeapp.models.res.SubCategory;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface PofApis {

    @POST("/api/oauth/register")
    Call<RegisterRes> authRegister(@Body RegisterReq req);

    @POST("/api/oauth/token")
    Call<User> authLogin(@Body LoginReq req);

    @GET("/api/public/category/list")
    Call<List<Category>> getAllCategories();

    @GET("/api/public/sub-category/list/{id}")
    Call<List<SubCategory>> getAllSubCategories(@Path("id") Integer id);

    @GET("/api/app/slide")
    Call<List<Slide>> getAllSlide();

    @GET("/api/public/post/list")
    Call<List<Post>> getAllProducts();

}
