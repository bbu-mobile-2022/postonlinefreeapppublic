package com.dinsaren.postonlinefreeapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dinsaren.postonlinefreeapp.R;
import com.dinsaren.postonlinefreeapp.models.res.Post;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostViewHolder>{
    private Context context;
    private List<Post> list;

    public PostAdapter(Context context, List<Post> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(
                R.layout.post_item_card_layout,
                null,
                false
        );
        return new PostViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder holder, int position) {
        Post post = list.get(position);
        Picasso.get().load(post.getImageUrl()).into(holder.imageView);
        holder.title.setText(post.getTitle());
        holder.price.setText("$ "+String.valueOf(post.getPrice()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class PostViewHolder extends RecyclerView.ViewHolder{
        TextView title,price;
        ImageView imageView;
        public PostViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.ivPostImage);
            title = itemView.findViewById(R.id.tvTitle);
            price = itemView.findViewById(R.id.tvPrice);
        }
    }
}
