package com.dinsaren.postonlinefreeapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dinsaren.postonlinefreeapp.R;
import com.dinsaren.postonlinefreeapp.models.res.SubCategory;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.SubCategoryViewHolder>{
    private Context context;
    private List<SubCategory> categoryList;

    public SubCategoryAdapter(Context context, List<SubCategory> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    @NonNull
    @Override
    public SubCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(
                R.layout.sub_category_item_card_layout,
                null, false
                );
        return new SubCategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubCategoryViewHolder holder, int position) {
        SubCategory category = categoryList.get(position);
        if(!category.getName().equals("") || null != category.getName()){
            holder.tvCategoryName.setText(category.getName());
        }
        Picasso.get().load(category.getImageUrl()).into(holder.imgCategoryImage);
    }

    @Override
    public int getItemCount() {
        if(!categoryList.isEmpty()){
           return categoryList.size();
        }
        return 0;
    }

    public static class SubCategoryViewHolder extends RecyclerView.ViewHolder{
        ImageView imgCategoryImage;
        TextView tvCategoryName;
        public SubCategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            imgCategoryImage = itemView.findViewById(R.id.categoryCardImage);
            tvCategoryName = itemView.findViewById(R.id.categoryCardName);
        }
    }
}
