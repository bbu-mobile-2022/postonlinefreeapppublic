package com.dinsaren.postonlinefreeapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dinsaren.postonlinefreeapp.adapters.CategoryAdapter;
import com.dinsaren.postonlinefreeapp.adapters.PostAdapter;
import com.dinsaren.postonlinefreeapp.adapters.SlideAdapter;
import com.dinsaren.postonlinefreeapp.app.BaseActivity;
import com.dinsaren.postonlinefreeapp.features.home.HomePresenter;
import com.dinsaren.postonlinefreeapp.features.home.HomeView;
import com.dinsaren.postonlinefreeapp.features.register.ui.LoginActivity;
import com.dinsaren.postonlinefreeapp.features.register.ui.RegisterActivity;
import com.dinsaren.postonlinefreeapp.models.res.Category;
import com.dinsaren.postonlinefreeapp.models.res.Post;
import com.dinsaren.postonlinefreeapp.models.res.Slide;
import com.dinsaren.postonlinefreeapp.utils.local.UserSharedPreference;

import java.util.List;

public class MainActivity extends BaseActivity implements HomeView {
    private Button btnOpenRegister,btnOpenLogin;
    private RecyclerView recyclerViewCategory, recyclerViewSlide, recyclerViewPost;
    private ProgressBar progressBar;
    private CategoryAdapter categoryAdapter;
    private SlideAdapter slideAdapter;
    private PostAdapter postAdapter;
    private HomePresenter homePresenter;
    private ConstraintLayout constraintlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnOpenRegister = findViewById(R.id.btnOpenRegister);
        btnOpenLogin = findViewById(R.id.btnOpenLogin);
        recyclerViewCategory = findViewById(R.id.recyclerViewCategory);
        recyclerViewSlide = findViewById(R.id.recyclerviewSlide);
        recyclerViewPost = findViewById(R.id.recyclerviewProducts);
        constraintlayout = findViewById(R.id.constraintlayout);

        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        homePresenter = new HomePresenter(this);
        homePresenter.getAllSlides();
        btnOpenRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        btnOpenLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!"".equals(UserSharedPreference.getUser(MainActivity.this).getUsername())){
                  UserSharedPreference.removeUser(MainActivity.this);
                }
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!"".equals(UserSharedPreference.getUser(this).getUsername())){
            btnOpenLogin.setText("Logout");
            btnOpenRegister.setVisibility(View.GONE);
        }else{
            btnOpenLogin.setText("Login");
            btnOpenRegister.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setCategoryList(List<Category> categoryList) {
        // Customer Layout manager
        if(!categoryList.isEmpty()) {
            GridLayoutManager layoutManager = new GridLayoutManager(this, 3){
                @Override
                public boolean checkLayoutParams(RecyclerView.LayoutParams lp) {
                   lp.width = getWidth() / getSpanCount();
                   return true;
                }

                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            };
            categoryAdapter = new CategoryAdapter(this, categoryList);
            recyclerViewCategory.setLayoutManager(layoutManager);
            recyclerViewCategory.setAdapter(categoryAdapter);
        }
        homePresenter.getAllProducts();
    }

    @Override
    public void setSlideList(List<Slide> slideList) {
        if(!slideList.isEmpty()){
           slideAdapter = new SlideAdapter(this,slideList);
            GridLayoutManager linearLayoutManager = new GridLayoutManager(
                    this,1,RecyclerView.HORIZONTAL,false
            );
            recyclerViewSlide.setLayoutManager(linearLayoutManager);
            recyclerViewSlide.setAdapter(slideAdapter);
        }
        homePresenter.getAllCategories();
    }

    @Override
    public void setProduct(List<Post> postList) {
        if(!postList.isEmpty()){
            postAdapter = new PostAdapter(this, postList);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2){
                @Override
                public boolean checkLayoutParams(RecyclerView.LayoutParams lp) {
                    lp.width = getWidth() / getSpanCount();
                    return true;
                }

                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            };
            recyclerViewPost.setLayoutManager(gridLayoutManager);
            recyclerViewPost.setAdapter(postAdapter);
        }
    }

    @Override
    public void onLoading(String message) {
        progressBar.setVisibility(View.VISIBLE);
        constraintlayout.setVisibility(View.GONE);
    }

    @Override
    public void onHideLoading(String message) {
        progressBar.setVisibility(View.GONE);
        constraintlayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(String message) {
        showMessage(message);
    }

    @Override
    public void onSuccess(String message) {
        showMessage(message);
    }

    @Override
    public void onInternalServerError(String message) {
        showMessage(message);
    }
}