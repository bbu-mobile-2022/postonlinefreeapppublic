package com.dinsaren.postonlinefreeapp.models.res;

import com.google.gson.annotations.SerializedName;

public class Post{

	@SerializedName("updatedBy")
	private int updatedBy;

	@SerializedName("modelId")
	private int modelId;

	@SerializedName("description")
	private Object description;

	@SerializedName("createdId")
	private int createdId;

	@SerializedName("subCategoryId")
	private int subCategoryId;

	@SerializedName("updatedDate")
	private Object updatedDate;

	@SerializedName("title")
	private String title;

	@SerializedName("categoryName")
	private String categoryName;

	@SerializedName("years")
	private int years;

	@SerializedName("condition")
	private String condition;

	@SerializedName("createdDate")
	private String createdDate;

	@SerializedName("locationId")
	private int locationId;

	@SerializedName("price")
	private double price;

	@SerializedName("imageUrl")
	private String imageUrl;

	@SerializedName("brandId")
	private int brandId;

	@SerializedName("id")
	private int id;

	@SerializedName("categoryId")
	private int categoryId;

	@SerializedName("username")
	private String username;

	@SerializedName("status")
	private String status;

	public void setUpdatedBy(int updatedBy){
		this.updatedBy = updatedBy;
	}

	public int getUpdatedBy(){
		return updatedBy;
	}

	public void setModelId(int modelId){
		this.modelId = modelId;
	}

	public int getModelId(){
		return modelId;
	}

	public void setDescription(Object description){
		this.description = description;
	}

	public Object getDescription(){
		return description;
	}

	public void setCreatedId(int createdId){
		this.createdId = createdId;
	}

	public int getCreatedId(){
		return createdId;
	}

	public void setSubCategoryId(int subCategoryId){
		this.subCategoryId = subCategoryId;
	}

	public int getSubCategoryId(){
		return subCategoryId;
	}

	public void setUpdatedDate(Object updatedDate){
		this.updatedDate = updatedDate;
	}

	public Object getUpdatedDate(){
		return updatedDate;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setCategoryName(String categoryName){
		this.categoryName = categoryName;
	}

	public String getCategoryName(){
		return categoryName;
	}

	public void setYears(int years){
		this.years = years;
	}

	public int getYears(){
		return years;
	}

	public void setCondition(String condition){
		this.condition = condition;
	}

	public String getCondition(){
		return condition;
	}

	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}

	public String getCreatedDate(){
		return createdDate;
	}

	public void setLocationId(int locationId){
		this.locationId = locationId;
	}

	public int getLocationId(){
		return locationId;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public double getPrice(){
		return price;
	}

	public void setImageUrl(String imageUrl){
		this.imageUrl = imageUrl;
	}

	public String getImageUrl(){
		return imageUrl;
	}

	public void setBrandId(int brandId){
		this.brandId = brandId;
	}

	public int getBrandId(){
		return brandId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setCategoryId(int categoryId){
		this.categoryId = categoryId;
	}

	public int getCategoryId(){
		return categoryId;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Post{" + 
			"updatedBy = '" + updatedBy + '\'' + 
			",modelId = '" + modelId + '\'' + 
			",description = '" + description + '\'' + 
			",createdId = '" + createdId + '\'' + 
			",subCategoryId = '" + subCategoryId + '\'' + 
			",updatedDate = '" + updatedDate + '\'' + 
			",title = '" + title + '\'' + 
			",categoryName = '" + categoryName + '\'' + 
			",years = '" + years + '\'' + 
			",condition = '" + condition + '\'' + 
			",createdDate = '" + createdDate + '\'' + 
			",locationId = '" + locationId + '\'' + 
			",price = '" + price + '\'' + 
			",imageUrl = '" + imageUrl + '\'' + 
			",brandId = '" + brandId + '\'' + 
			",id = '" + id + '\'' + 
			",categoryId = '" + categoryId + '\'' + 
			",username = '" + username + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}