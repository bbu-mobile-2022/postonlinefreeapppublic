package com.dinsaren.postonlinefreeapp.utils.local;

import android.content.Context;
import android.content.SharedPreferences;

import com.dinsaren.postonlinefreeapp.models.User;

public class UserSharedPreference {
    public static void setUser(Context context, User data){
        SharedPreferences.Editor editor = context.getSharedPreferences("USER", Context.MODE_PRIVATE).edit();
        editor.putInt("ID", data.getId());
        editor.putString("USERNAME", data.getUsername());
        editor.putString("EMAIL", data.getEmail());
        editor.putString("PHONE",data.getPhone());
        editor.putString("ACCESS_TOKEN",data.getAccessToken());
        editor.putString("REFRESH_TOKEN", data.getRefreshToken());
        editor.apply();
    }

    public static User getUser(Context context){
        SharedPreferences preferences = context.getSharedPreferences("USER",Context.MODE_PRIVATE);
        User user = new User();
        user.setId(preferences.getInt("ID",0));
        user.setUsername(preferences.getString("USERNAME",""));
        user.setEmail(preferences.getString("EMAIL",""));
        user.setPhone(preferences.getString("PHONE",""));
        user.setAccessToken(preferences.getString("ACCESS_TOKEN",""));
        user.setRefreshToken(preferences.getString("REFRESH_TOKEN",""));
        return user;
    }

    public static void removeUser(Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences("USER",Context.MODE_PRIVATE).edit();
        editor.remove("ID");
        editor.remove("USERNAME");
        editor.remove("EMAIL");
        editor.remove("PHONE");
        editor.remove("ACCESS_TOKEN");
        editor.remove("REFRESH_TOKEN");
        editor.apply();
    }

}
