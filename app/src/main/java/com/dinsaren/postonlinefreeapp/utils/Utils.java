package com.dinsaren.postonlinefreeapp.utils;

import com.dinsaren.postonlinefreeapp.api.PofApis;
import com.dinsaren.postonlinefreeapp.api.PofClients;

public class Utils {
    public static PofApis getPofApis() {
        return PofClients.getPofClient().create(PofApis.class);
    }

}

